package com.sda;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-17.
 */
public class Primes {
    ArrayList<Integer> myList = new ArrayList<Integer>();

    public void getPrimes(int a) {
        for (int i = 1; i <= a; i++) {
            int factors = 0;
            int j = 1;

            while (j <= i) {
                if (i % j == 0) {
                    factors++;
                }
                j++;
            }
            if (factors == 2) {
                System.out.println(i);
            }
        }
    }

    void printPrimes(int n) {
        for (int i = 2; i < n; i++) {
            if (isPrime(i)) {
                System.out.println(i);
                myList.add(i);
            }
        }

    }

    public int countPrimes(int begin, int end) {
        int primeCounter = 0;
        for (int i = begin; i < end; i++) {
            if (isPrime(i)) {
                primeCounter++;
            }
        }
        System.out.println("Primes in comparment:" + primeCounter);
        return primeCounter;
    }

    boolean isPrime(int l) {
        if (l < 2) {
            return false;
        }
        for (int i = 2; i * i <= l; i++) {
            if (l % i == 0) {
                return false;
            }
        }

        return true;
    }

}